(function ($, Drupal, drupalSettings) {
	Drupal.behaviors.dexp_block_settings_video_background = {
    attach: function (context, settings) {
			$('.dexp-video-background').once('YTPlayer').each(function(){
				var $this = $(this);
        $(this).YTPlayer();
				$(this).find('.yt-control').each(function(){
          $(this).on('click', function(){
            var func = $(this).data('action');
            try{
              $this[func].apply($this);
            }catch(e){}
          });
        });
			});
		}
	};
})(jQuery, Drupal, drupalSettings);
<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Provides a shortcode for gmap.
 *
 * @Shortcode(
 *   id = "dexp_builder_gmap",
 *   title = @Translation("Gmap"),
 *   description = @Translation("Builds gmap element"),
 *   group = @Translation("Content"),
 *   child = {"dexp_builder_gmap_marker"},
 * )
 */
class BuilderGmap extends BuilderElement {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    global $builder_gmap_stack;
    $attrs = $this->getAttributes(array(
      'height' => '',
      'custom_style' => '',
      'zoom' => '',
      ), $attributes
    );
    $output = [
      '#theme' => 'dexp_builder_gmap',
      '#height' => $attrs['height'],
      '#zoom' => $attrs['zoom'],
      '#markers' => $builder_gmap_stack,
      '#custom_style' => $attrs['custom_style'],
      '#attached' => array(
        'library' => array(
          'dexp_builder/gmap'
        ),
      )
    ];
    $builder_gmap_stack = array();
    return $this->render($output);
  }
  
  public function processBuilder($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    parent::process($attributes, $text, $langcode);

    $output = [
      '#markup' => $text,
      '#attached' => ['library' => ['dexp_builder/gmap-api']],
    ];
    return $this->render($output);
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['general_options']['height'] = array(
      '#type' => 'number',
      '#title' => $this->t('Height'),
      '#default_value' => $this->get('height', 400),
    );
    $form['general_options']['zoom'] = array(
      '#type' => 'number',
      '#title' => $this->t('Zoom Level'),
      '#default_value' => $this->get('zoom', 14),
    );
    $form['general_options']['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class', ''),
    );
    $form['advanced'] = array(
      '#type' => 'details',
      '#title' => $this->t('Custom style'),
      '#group' => 'element_settings',
    );
    $form['advanced']['custom_style'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Gmap custom style'),
      '#description' => $this->t('Buil your style at <a href="https://mapstyle.withgoogle.com/" target="_blank">https://mapstyle.withgoogle.com/</a> and paste json code here'),
      '#default_value' => $this->get('custom_style'),
    );
    unset($form['animate_options']);
    unset($form['design_options']);
    return $form;
  }

}

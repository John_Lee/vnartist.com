<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Provides a shortcode gmap marker icon.
 *
 * @Shortcode(
 *   id = "dexp_builder_gmap_marker",
 *   title = @Translation("Gmap Marker"),
 *   description = @Translation("Builds gmap marker element"),
 *   group = @Translation("Content"),
 *   parent = {"dexp_builder_gmap"},
 *   child = {}
 * )
 */
class BuilderGmapMarker extends BuilderElement {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => '',
      'address' => '',
      'lng' => '',
      'lat' => '',
      'icon' => '',
      'image' => '',
      'class' => '',
      'phone' => '',
      ), $attributes
    );
    global $builder_gmap_stack;
    if($attrs['icon']){
      $fid = str_replace('file:', '', $attrs['icon']);
      $file = \Drupal\file\Entity\File::load($fid);
      $attrs['icon'] = file_create_url($file->getFileUri());
    }
    if (empty($builder_gmap_stack)) {
      $builder_gmap_stack = array();
    }
    $builder_gmap_stack[] = $attrs;
    return '';
  }
  
  public function processBuilder($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => '',
      'address' => '',
      'image' => '',
      'class' => '',
      ), $attributes
    );
    $output = [
      '#markup' => '<span class="fa fa-map-marker"></span> ' . $attrs['title'] . ':' . $attrs['address'],
      '#attached' => ['library' => ['dexp_builder/gmap-api']],
    ];
    return $this->render($output);
  }


  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['general_options']['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->get('title', ''),
      '#required' => true,
    );
    $form['general_options']['address'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Address'),
      '#default_value' => $this->get('address', ''),
      '#required' => true,
    );
    $form['general_options']['lat'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Longitude'),
      '#default_value' => $this->get('lat', ''),
      '#required' => true,
    );
    $form['general_options']['lng'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Latitude'),
      '#default_value' => $this->get('lng', ''),
      '#required' => true,
    );
    $form['general_options']['find_ln_lg'] = array(
      '#type' => 'button',
      '#value' => 'Find longitude, latitude from address',
      '#id' => 'dexp-builder-gmap-marker-find-ln-lg',
    );
    $form['general_options']['gmap_preview'] = array(
      '#markup' => '<div id="dexp-builder-gmap-preview"></div>',
    );
    $form['general_options']['icon'] = array(
      '#type' => 'image_browser',
      '#title' => $this->t('Marker Icon'),
      '#default_value' => $this->get('icon'),
    );
    $form['general_options']['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class', ''),
    );
    $form['#attached']['library'][] = 'dexp_builder/gmap-admin';
    unset($form['animate_options']);
    return $form;
  }

}

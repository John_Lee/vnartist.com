<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Language\Language;

/**
 * Provides a shortcode for skillbar.
 *
 * @Shortcode(
 *   id = "dexp_builder_skillbar",
 *   title = @Translation("Skillbar"),
 *   description = @Translation("Skillbar"),
 *   group = @Translation("Content"),
 *   child = {}
 * )
 */
class Skillbar extends BuilderElement {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    parent::process($attributes, $text, $langcode);

    $attrs = $this->getAttributes(array(
      'title' => '',
      'icon' => '',
      'percent' => '0',
      'class' => '',
        ), $attributes
    );

    $return = array(
      '#theme' => 'dexp_builder_skillbar',
      '#title' => $attrs['title'],
      '#icon' => $attrs['icon'],
      '#percent' => $attrs['percent'],
      '#class' => $attrs['class'],
      '#attached' => ['library' => ['dexp_builder/skillbar']],
    );
    return $this->render($return);
  }

  public function settingsForm(array $form, \Drupal\Core\Form\FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['general_options']['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->get('title'),
    );
    $form['general_options']['icon'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->get('icon', ''),
      '#attributes' => ['class' => ['icon-select']],
    );
    $form['general_options']['percent'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Pecent'),
      '#default_value' => $this->get('percent'),
    );

    $form['general_options']['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class'),
    );

    return $form;
  }

  public function processBuilder($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => '',
      'icon' => '',
      'percent' => '0',
      'class' => '',
        ), $attributes
    );
    return '[Skillbar: ' . $attrs['title'] . ' ' . $attrs['percent'] . '%]';
  }

}
<?php

namespace Drupal\dexp_builder\Plugin\Shortcode;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\Language;

/**
 * Provides a shortcode for icon.
 *
 * @Shortcode(
 *   id = "dexp_builder_icon",
 *   title = @Translation("Icon"),
 *   description = @Translation("Builds icon element"),
 *   group = @Translation("Content"),
 *   child = {},
 * )
 */
class BuilderIcon extends BuilderElement {

  public function process($attributes, $text, $langcode = Language::LANGCODE_NOT_SPECIFIED) {
    $attrs = $this->getAttributes(array(
      'title' => '',
      'icon' => '',
      'icon_library' => '',
      'font_size' => '',
      'link' => '',
      'link_target' => '_self',
      'class' => '',
      'tooltip' => '',
        ), $attributes
    );

    $attribute = new \Drupal\Core\Template\Attribute();
    $attribute->addClass('dexp-icon');
    $attribute->addClass($attrs['icon']);
    $attribute->addClass($attrs['class']);
    if ($attrs['font_size']) {
      $attribute->setAttribute('style', 'font-size: ' . $attrs['font_size']);
    }
    $link = '';
    if($attrs['link']){
      if($attrs['link'] == '#'){
        $link = $attrs['link'];
      }else{
        try{
          $link = \Drupal\Core\Url::fromUserInput($attrs['link'])->toString();
        }catch (\Exception $e){
          $link = $attrs['link'];
        }
      }
    }
    if ($attrs['tooltip']) {
      $attribute->setAttribute('data-placement', 'auto');
      $attribute->setAttribute('data-toggle', 'tooltip');
      $attribute->setAttribute('title', $attrs['tooltip']);
    }
    $output = array(
      '#theme' => 'dexp_builder_icon',
      '#title' => $attrs['title'],
      '#link' => $link,
      '#link_target' => $attrs['link_target'],
      '#attributes' => $attribute,
    );
    if ($attrs['icon_library'] && ($icon_plugin = \Drupal::service('dexp_builder.fonticon')->getFontIconPlugin($attrs['icon_library']))) {
      $output['#attached']['library'][] = $icon_plugin->library();
    }
    if($attrs['link_target'] == 'popup'){
      $attribute->addClass('dexp-video-popup');
      $output['#link_target'] = '_self';
    }
    if (strpos($attrs['class'], 'dexp-video-popup') !== false) {
      $output['#attached']['library'][] = 'dexp_builder/video-popup';
    }
    return $this->render($output);
  }

  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['general_options']['icon'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Icon'),
      '#default_value' => $this->get('icon', ''),
      '#attributes' => ['class' => ['icon-select']],
    );
    $form['icon_library'] = array(
      '#type' => 'hidden',
      '#default_value' => $this->get('icon_library', ''),
    );
    $form['general_options']['font_size'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Font size'),
      '#default_value' => $this->get('font_size', ''),
    );
    $form['general_options']['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => $this->get('title', ''),
    );
    $form['general_options']['link'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Link'),
      '#description' => $this->t('Add link to icon'),
      '#default_value' => $this->get('link', '#'),
    );
    $form['general_options']['link_target'] = array(
      '#type' => 'select',
      '#options' => [
        '' => $this->t('Same window'),
        '_blank' => $this->t('New window'),
      ],
      '#title' => $this->t('Link target'),
      '#default_value' => $this->get('link_target'),
    );
    if(\Drupal::service('module_handler')->moduleExists('colorbox')){
      $form['general_options']['link_target']['#options']['popup'] = $this->t('Popup');
    }
    $form['general_options']['tooltip'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Tooltip'),
      '#default_value' => $this->get('tooltip', ''),
    );
    $form['general_options']['class'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Custom class'),
      '#default_value' => $this->get('class', ''),
    );

    unset($form['animate_options']);
    return $form;
  }

}

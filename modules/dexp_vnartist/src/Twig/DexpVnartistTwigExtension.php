<?php

/**
 * @file
 * Contains \Drupal\dexp_vnartist\src\Twig\DexpVnartistTwigExtension.
 */

use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Entity;

namespace Drupal\dexp_vnartist\Twig;

class DexpVnartistTwigExtension extends \Twig_Extension {

    public function debug($var) {
        return dsm($var);
    }
    public function product_comment_count($product_entity){
        $comment_count = '0';
        $entity = $product_entity;
        $static = \Drupal::service('comment.statistics')->read(array($entity->id()=>$entity), 'commerce_product');
        if(!empty($static)){
            $comment_count = $static[0]->comment_count;
        }
        return $comment_count;
    }
    public function embed_view($view_name, $view_machine, $param){
        return views_embed_view($view_name, $view_machine, $param);
    }
    public function current_uid(){
        return \Drupal::currentUser()->id();
    }
	public function is_admin(){
        $uid = \Drupal::currentUser()->id();
		if($uid !=0){
			$user = \Drupal\user\Entity\User::load($uid);
			$roles = $user->getRoles();
			$is_admin = false;
			foreach($roles as $r){
				if($r == 'administrator'){
					$is_admin = true;
				}
			}
			return $is_admin;
		} else {
			return false;
		}
    }
    public function get_uid_from_pid($pid){
        $product = \Drupal\commerce_product\Entity\Product::load($pid);
        $uid = $product->uid->getValue()[0]['target_id'];
        return $uid;
    }
    public function wishlist_product(){
        $block_manager = \Drupal::service('plugin.manager.block');
        $wlp_block = $block_manager->createInstance('wishlist_product_block', array());
        $render = $wlp_block->build();
        return $render;
    }
    
    public function getUrlProductStyle($style, $uri){
        $transform = array();
        $transform[$style] = \Drupal\image\Entity\ImageStyle::load($style)->buildUrl($uri);
        return $transform[$style];
    }

    /**
    * {@inheritdoc}
    */
    public function getFunctions() {
        return [
            new \Twig_SimpleFunction('debug', array($this, 'debug')),
            new \Twig_SimpleFunction('product_comment_count', array($this, 'product_comment_count')),
            new \Twig_SimpleFunction('embed_view', array($this, 'embed_view')),
            new \Twig_SimpleFunction('current_uid', array($this, 'current_uid')),
			new \Twig_SimpleFunction('is_admin', array($this, 'is_admin')),
            new \Twig_SimpleFunction('get_uid_from_pid', array($this, 'get_uid_from_pid')),
            new \Twig_SimpleFunction('wishlist_product', array($this, 'wishlist_product')),
            new \Twig_SimpleFunction('getUrlProductStyle', array($this, 'getUrlProductStyle')),
        ];
    }

    /**
    * Returns the name of the extension.
    *
    * @return string The extension name
    */
    public function getName() {
        return 'DexpVnartistTwigExtensions';
    }

}

<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an User Product block.
 *
 * @Block(
 *   id = "user_product_block",
 *   admin_label = @Translation("User Product block"),
 * )
 */
class UserProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		
		$current_path = \Drupal::service('path.current')->getPath();
		$path_args = explode('/', $current_path);
		if($path_args[1] == 'product'){
			$product_id = $path_args[2];
		}
		if($product_id){
			$product = \Drupal\commerce_product\Entity\Product::load($product_id);
			$uid = $product->uid->getValue()[0]['target_id'];
			$user = \Drupal\user\Entity\User::load($uid);
			$name = $user->get('field_ho_va_ten')->value;
			$image = $user->user_picture->view('user');
		}
		if(!empty($uid)){
			$view = views_embed_view('product', 'user_product', $uid);
		}
		
		return array(
			'#theme' => 'user_product_block',
			'#uid' => $uid,
			'#image' => $image,
			'#name' => $name,
			'#view' => $view,
		);
	}
}

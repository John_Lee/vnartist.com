<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Profile Product block.
 *
 * @Block(
 *   id = "profile_product_block",
 *   admin_label = @Translation("Profile Product block"),
 * )
 */
class ProfileProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		
		$current_path = \Drupal::service('path.current')->getPath();
		$path_args = explode('/', $current_path);
		if($path_args[1] == 'user'){
			$user_id = $path_args[2];
		}
		if(!empty($user_id)){
			$view = views_embed_view('product', 'profile_product', $user_id);
		}
		
		return array(
			'#type' => 'markup',
			'#markup' => render($view),
		);
	}
}

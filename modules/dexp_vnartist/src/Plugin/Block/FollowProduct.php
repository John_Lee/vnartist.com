<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Follow Product block.
 *
 * @Block(
 *   id = "follow_product_block",
 *   admin_label = @Translation("Follow Product block"),
 * )
 */
class FollowProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		$uid = \Drupal::currentUser()->id();
		$query_fu = \Drupal::database()->select('flagging', 'fl');
		$query_fu->fields('fl', ['entity_id']);
		$query_fu->condition('fl.uid', $uid, '=');
		$query_fu->condition('fl.flag_id', 'following', '=');
		$result_fu = $query_fu->execute();
		$flag_user = array();
		while ($row_fu = $result_fu->fetchAssoc()) {
			if($row_fu['entity_id'] != 0){
				$flag_user[] = $row_fu['entity_id'];
			}
		}
		
		if(!empty($uid)){
			$view = views_embed_view('front_page', 'follow_product', implode(',', $flag_user));
		}
		
		return array(
			'#type' => 'markup',
			'#markup' => render($view),
		);
	}
}

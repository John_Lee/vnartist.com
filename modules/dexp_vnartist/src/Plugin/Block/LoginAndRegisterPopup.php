<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;
use \Drupal\Core\Link;
use Drupal\Component\Serialization\Json;


/**
 * Provides a 'LoginAndRegisterPopup' block.
 *
 * @Block(
 *  id = "login_register_form_popup",
 *  admin_label = @Translation("Login And Register Form popup"),
 * )
 */
class LoginAndRegisterPopup extends BlockBase {


	/**
	* {@inheritdoc}
	*/
	public function build() {
		$url_register = Url::fromRoute('user.register');
		$url_login = Url::fromRoute('user.login');
		$link_options_register = array(
			'attributes' => array(
				'class' => array(
					'use-ajax',
					'register-popup-form',
				),
				'data-dialog-type' => 'modal',
				'data-dialog-options' => Json::encode(['width' => 400]),
			),
		);
		$link_options_login = array(
			'attributes' => array(
				'class' => array(
					'use-ajax',
					'login-popup-form',
				),
				'data-dialog-type' => 'modal',
				'data-dialog-options' => Json::encode(['width' => 350]),
			),
		);
		$url_register->setOptions($link_options_register);
		$url_login->setOptions($link_options_login);
		$link_register = Link::fromTextAndUrl(t('Đăng kí'), $url_register)->toString();
		$link_login = Link::fromTextAndUrl(t('Đăng nhập'), $url_login)->toString();
		$build = [];
		if (\Drupal::currentUser()->isAnonymous()) {
			$build['login_register_popup_block']['#markup'] = '<div class="login-register-popup-link"><span>' . $link_login . '' . $link_register . '</span></div>';
		}
		$build['login_register_popup_block']['#attached']['library'][] = 'core/drupal.dialog.ajax';

		return $build;
	}
}

<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Wishlist Product block.
 *
 * @Block(
 *   id = "wishlist_product_block",
 *   admin_label = @Translation("Wishlist Product block"),
 * )
 */
class WishlistProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		
		$current_path = \Drupal::service('path.current')->getPath();
		$path_args = explode('/', $current_path);
		if($path_args[1] == 'user'){
			$user_id = $path_args[2];
		}
        
        if(isset($user_id)){
            $query = \Drupal::database()->select('commerce_product_field_data', 'cpfd');
            $query->fields('cpfd', ['product_id']);
            $query->join('commerce_wishlist_item', 'cwli', 'cwli.title = cpfd.title');
            $query->join( 'commerce_wishlist', 'cwl', 'cwl.wishlist_id = cwli.wishlist_id');
            $query->condition('cwl.uid', $user_id, '=');
            $result = $query->execute();
            $output = array();
            while ($row = $result->fetchAssoc()) {
                $output[] = $row['product_id'];
            }
            if(!empty($output)){
                $view = views_embed_view('front_page', 'product_wishlist', implode(",", $output));
            }
        }
        
        if(isset($view)){
            return array(
                '#type' => 'markup',
                '#markup' => render($view),
            );
        } else {
            return array(
                '#type' => 'markup',
                '#markup' => 'Chưa có ảnh đã lưu.',
            );
        }
	}
}

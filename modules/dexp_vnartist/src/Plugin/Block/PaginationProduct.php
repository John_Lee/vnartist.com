<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Pagination Product block.
 *
 * @Block(
 *   id = "pagination_product_block",
 *   admin_label = @Translation("Pagination Product block"),
 * )
 */
class PaginationProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		
		$connection = \Drupal::database();
		$query = $connection->query("SELECT product_id FROM {commerce_product}");
		$result = $query->fetchAll();
		$list_pid = array();
		foreach($result as $r){
			$list_pid[] = $r->product_id;
		}
		$current_path = \Drupal::service('path.current')->getPath();
		$path_args = explode('/', $current_path);
		if($path_args[1] == 'product'){
			$product_id = $path_args[2];
		}
		$vt = 0;
		if($product_id){
			for($i=0;$i<count($list_pid);$i++){
				if($list_pid[$i] == $product_id){
					$vt = $i;
				}
			}
		}
		$nav = '<div class="product-pagination">';
		if($vt !=0 && $vt < count($list_pid) - 1){
			$product_prev = \Drupal\commerce_product\Entity\Product::load($list_pid[$vt - 1]);
			$product_next = \Drupal\commerce_product\Entity\Product::load($list_pid[$vt + 1]);
            if(isset($product_prev->path->getValue()[0]['alias'])){
                $nav .= '<a class="btn-prev" href="'.$product_prev->path->getValue()[0]['alias'].'"><i class="fa fa-caret-left"></i>Prev</a>';
            } else {
                $nav .= '<a class="btn-prev" href="/"><i class="fa fa-caret-left"></i>Prev</a>';
            }
			$nav .= '<a class="btn-all" href="/"><i class="fa fa-th"></i>All</a>';
            if(isset($product_next->path->getValue()[0]['alias'])){
                $nav .= '<a class="btn-next" href="'.$product_next->path->getValue()[0]['alias'].'">Next<i class="fa fa-caret-right"></i></a>';
            } else {
                $nav .= '<a class="btn-next" href="/">Next<i class="fa fa-caret-right"></i></a>';
            }
		} elseif($vt == 0){
			$product_next = \Drupal\commerce_product\Entity\Product::load($list_pid[$vt + 1]);
			$nav .= '<a class="btn-prev" href=""><i class="fa fa-caret-left"></i>Prev</a>';
			$nav .= '<a class="btn-all" href="/"><i class="fa fa-th"></i>All</a>';
			if(isset($product_next->path->getValue()[0]['alias'])){
                $nav .= '<a class="btn-next" href="'.$product_next->path->getValue()[0]['alias'].'">Next<i class="fa fa-caret-right"></i></a>';
            } else {
                $nav .= '<a class="btn-next" href="/">Next<i class="fa fa-caret-right"></i></a>';
            }
		} elseif($vt == count($list_pid) - 1){
			$product_prev = \Drupal\commerce_product\Entity\Product::load($list_pid[$vt - 1]);
			if(isset($product_prev->path->getValue()[0]['alias'])){
                $nav .= '<a class="btn-prev" href="'.$product_prev->path->getValue()[0]['alias'].'"><i class="fa fa-caret-left"></i>Prev</a>';
            } else {
                $nav .= '<a class="btn-prev" href="/"><i class="fa fa-caret-left"></i>Prev</a>';
            }
			$nav .= '<a class="btn-all" href="/"><i class="fa fa-th"></i>All</a>';
			$nav .= '<a class="btn-next" href="">Next<i class="fa fa-caret-right"></i></a>';
		}
		$nav .= '</div>';
		return array(
			'#type' => 'markup',
			'#markup' => render($nav),
		);
	}
}

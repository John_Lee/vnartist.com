<?php

namespace Drupal\dexp_vnartist\Plugin\Block;

use Drupal\Core\Block\BlockBase;

/**
 * Provides an Related Product block.
 *
 * @Block(
 *   id = "related_product_block",
 *   admin_label = @Translation("Related Product block"),
 * )
 */
class RelatedProduct extends BlockBase {

	/**
	* {@inheritdoc}
	*/
	public function build() {
		
		$current_path = \Drupal::service('path.current')->getPath();
		$path_args = explode('/', $current_path);
		if($path_args[1] == 'product'){
			$product_id = $path_args[2];
		}
		if($product_id){
			$product = \Drupal\commerce_product\Entity\Product::load($product_id);
			$cats = $product->field_chon_categories->getValue();
			$tid = array();
			if(!empty($cats)){
				foreach($cats as $cat){
					$tid[] = $cat['target_id'];
				}
			}
		}
		if(!empty($tid)){
			$view = views_embed_view('product', 'related_product', implode(",", $tid));
		}
		
		return array(
			'#type' => 'markup',
			'#markup' => render($view),
		);
	}
}

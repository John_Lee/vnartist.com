<?php  
/**
 * @file
 * Contains \Drupal\dexp_vnartist\Plugin\QueueWorker\VnartistQueueWorker.
 */

namespace Drupal\dexp_vnartist\Plugin\QueueWorker;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Processes tasks for dexp_vnartist module.
 *
 * @QueueWorker(
 *   id = "vnartist_queue",
 *   title = @Translation("Vnartist Queue worker"),
 *   cron = {"time" = 1440}
 * )
 */
class VnartistQueueWorker extends QueueWorkerBase {

	/**
	* {@inheritdoc}
	*/
	public function processItem($item) {
		if(!empty($item)){
			\Drupal::database()
			->delete('flagging')
			->condition('uid', 0, '=')
			->execute();
		}
	}
}

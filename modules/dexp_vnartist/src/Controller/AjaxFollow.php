<?php

/**
 * @file
 * Contains \Drupal\dexp_vnartist\Controller\AjaxFollow.
 */

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

namespace Drupal\dexp_vnartist\Controller;

class AjaxFollow {
	
	public function refeshfollow() {
		/* User Follow */
		$query_uf = \Drupal::database()->select('flagging', 'fl');
		$query_uf->fields('fl', ['uid']);
		$query_uf->condition('fl.entity_id', 1, '=');
		$query_uf->condition('fl.flag_id', 'following', '=');
		$result_uf = $query_uf->execute();
		$user_flag = '';
		$count_uf = 0;
		while ($row_uf = $result_uf->fetchAssoc()) {
			if($row_uf['uid'] != 0){
				$account_uf = \Drupal\user\Entity\User::load($row_uf['uid']); 
				$user_flag .= '<li><a href="'. \Drupal::service('path.alias_manager')->getAliasByPath("/user/". $row_uf['uid']) .'">'. $account_uf->get('field_ho_va_ten')->value .'</a></li>';
				$count_uf ++;
			}
		}
		$count_user_follow = '<span class="number">'. $count_uf . '</span> '. t('Người theo dõi bạn');
		$user_follow_list = '<ul class="user-follow-list">'. $user_flag . '</ul>';

		return new JsonResponse($count_user_follow);
	}
}

<?php
 
namespace Drupal\custom\Controller;
 
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\AlertCommand;
use Drupal\Core\Controller\ControllerBase;
use Drupal\file\Entity\File;
use Drupal\Core\Ajax\DataCommand;
use Drupal\Core\Ajax\BaseCommand;
class CustomAjaxUploadController extends ControllerBase {
 
  public function AjaxUploadFile() {
		$file_temp = file_unmanaged_move($_FILES['file']['tmp_name'], 'public://' . $_FILES['file']['name']);
		$file = \Drupal\file\Entity\File::create(['uri' => $file_temp, 'uid' => \Drupal::currentUser()->id()]);
		$file->setTemporary();
		$file->save();
    $response = new AjaxResponse();
    # Commands Ajax
    $response->addCommand(new BaseCommand ('File Name', ['fid' => $file->id(), 'file_url' => file_create_url($file->getFileUri())]));
    # Return response
    return $response;
 
  }
 
}
<?php

/**
 * @file
 * Contains Drupal\cútom\Form\CustomAddPictureMultiStepForm.
 */

namespace Drupal\custom\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

class CustomAddPictureMultiStepForm extends FormBase
 {

  protected $step = 1;

  /**
   * {@inheritdoc}
   */
  public function getFormID() {
    return 'custom_pcture_add';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
		//$form['#attributes'] = array('enctype' => "multipart/form-data");
		$form = [];
		$form['header_title'] = [
			'#markup' => '<div class="header-title"><h2>'. $this->t('Đăng Tranh') .'</h2></div>',
		];
		$form['picture_file'] = [
			'#title' => $this->t('Hình ảnh'),
			'#type' => 'hidden',
			'#prefix' => '<div class="custom-input-upload-file"><input type="file" name="file" />
				<div class="dz-message needsclick">
					<span class="note needsclick">'. $this->t('Kéo thả hình ảnh ') .'</span><br>
					Hoặc<br>
					<span class="btn">Upload từ máy tính</span>
				</div>
			</div>
			',
			'#attributes' => [
				'class' => ''
			],
		];
		
		$form['container_left'] = [
			'#type' => 'container',
			'#attributes' => [
				'class' => ['col-md-8'],
			],
			'#prefix' => '<div class="row">'
		];
		$form['container_left']['display_image_style'] = [
			'#type' => 'select',
			'#title' => $this->t('Chọn chất lượng hình ảnh'),
			'#default_value' => 1,
			'#prefix' => '<div class="wrapper-image-picture"><div class="content-image"></div>',
			'#suffix' => '</div>',
			'#options' => [
				'1' => $this->t('Ảnh Gốc'),
				'2' => $this->t('Size: 400 pixel wide'),
				'3' => $this->t('Size: 600 pixel wide'),
				'4' => $this->t('Size: 800 pixel wide'),
				'5' => $this->t('Size: 900 pixel wide'),
				'6' => $this->t('Size: 1024 pixel wide'),
				'7' => $this->t('Size: 1600 pixel wide'),
			]
		];
		$form['container_left']['title'] = [
			'#type' => 'textfield',
			'#title' => $this->t(''),
			'#attributes' => [
				'placeholder' => $this->t('Tiêu đề'),
			],
			'#required' => TRUE,
		];
		$form['container_left']['description'] = [
			'#type' => 'textarea',
			'#title' => $this->t(''),
			'#attributes' => [
				'placeholder' => $this->t('Mô tả'),
			],
			//required' => TRUE,
		];
		$url = \Drupal\Core\Url::fromRoute('custom.open_modal')->toString();
		$form['container_left']['policy'] = [
			'#type' => 'checkboxes',
			'#title' => '',
			'#options' => [
				'1' => $this->t('Xem điều khoản <a href="@link" class="use-ajax">tại đây</a>', ['@link' => $url])
			]
		];

		$form['container_right'] = [
			'#type' => 'container',
			'#attributes' => [
				'class' => ['col-md-4'],
			],
			'#suffix' => '</div>',
		];

		$form['container_right']['download_file'] = [
			'#type' => 'checkboxes',
			'#title' => $this->t(''),
			'#options' => [
				'1' => $this->t('Cho phép Download')
			]
		];

		$item_img_info = '<div class="d-print-table border">
						<div class="d-print-table-cell">
							<div class="img-thumbnail"></div>
						</div>
						<div class="d-print-table-cell">
							<span class="size-img"></span>
							<span class="size-img"></span>
						</div>
					</div>';
		$form['container_right']['item_download_file'] = [
			'#markup' => $item_img_info,
		];

		$form['container_right']['categories'] = [
			'#type' => 'select',
			'#title' => $this->t('Chọn Category'),
			'#multiple' => 'true',
			'#options' => $this->getAllTaxonomyCategories('categories'),
			'#attributes' => [
				'class' => ['js-select2-categories'],
				'data-title-placeholder' => $this->t('Tìm kiếm category')
			],
			'#required' => TRUE,
		];
		$form['container_right']['dung_cu'] = [
			'#type' => 'textfield',
			'#title' => $this->t('Dụng cụ sử dụng'),
			'#required' => TRUE,
		];
		$form['container_right']['actions'] = [
			'#type' => 'container',
			'#attributes' => [
				'class' => ['wrapper-form-action'],
			],
		];
		$form['container_right']['actions']['submit'] = [
			'#type' => 'submit',
			'#value' => $this->t('Đăng Ngay'),
			'#validate' => ['::CustomSubmitFormAddValidate'],
			'#submit' => array('::SubmitFormAddPicture'),
		];
		$form['container_right']['actions']['huy'] = [
			'#type' => 'submit',
			'#value' => $this->t('Hủy'),
			'#submit' => array('::SubmitFormCancel'),
		];
    
		$form['#attached']['library'] = [
			'dropzonejs/dropzonejs',
			'custom/Select2',
			'custom/file_upload',
			'core/drupal.dialog.ajax'
		];

		$form['#cache']['max-age'] = 0;
		$form['#attributes']['class'] = ['custom-dropzone wrapper-form-add-picture form-step-1 form-step-2'];
    return $form;
	}
	
	public function getAllTaxonomyCategories($vid) {
		$terms =\Drupal::entityTypeManager()->getStorage('taxonomy_term')->loadTree($vid);
		$options = [];
		foreach ($terms as $term) {
			$options[$term->tid] = $term->name;
		}
		return $options;
	}

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
	}


	public function CustomSubmitFormAddValidate(array &$form, FormStateInterface $form_state) {
		$values = $form_state->getValues();
		if( empty( $values['picture_file'] ) ) {
			$form_state->setErrorByName('picture_file', $this->t('Vui lòng upload hình!'));
		}
		if( mb_strlen( $values['description'] ) > 300 ) {
			$form_state->setErrorByName('description', $this->t('Mô tả không quá 300 ký tự!'));
		}
		if( $values['policy'][1] == 0 ) {
			$form_state->setErrorByName('policy', $this->t('Vui lòng đọc điều khoản!'));
		}
	}
	
	

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    if($this->step < 2) {
      //$form_state->setRebuild();
      //$this->step++;
    }
    else {
    }
	}
	
	/**
   * {@inheritdoc}
   */
  public function SubmitFormAddPicture(array &$form, FormStateInterface $form_state) {
		$values = $form_state->getValues();
		//print_r($values); die();
		$file = \Drupal\file\Entity\File::load( $values['picture_file'] );
		$file->setFilename($values['title']);
		$file->setPermanent();
		$file->save();
		$product = \Drupal\commerce_product\Entity\Product::create(['type'=>'pictures']);
		$product->setTitle($values['title']);
		$product->set('uid', \Drupal::currentUser()->id());
		$product->set('field_chon_categories', $values['categories']);
		$product->set('field_dung_cu', $values['dung_cu']);
		$product->set('field_description', $values['description']);
		$product->set('field_picture_image', $values['picture_file']);
		$product->set('field_hien_thi_hinh_anh', $values['display_image_style']);
		if( $values['download_file'][1] == 1 ) {
			$product->set('field_cho_phep_download', $values['download_file']);
		}
		
		$product->save();
		$url = \Drupal\Core\Url::fromRoute('entity.commerce_product.canonical', ['commerce_product' => $product->id()]);
		$form_state->setRedirectUrl($url);
  }


	/**
   * {@inheritdoc}
   */
  public function SubmitFormCancel(array &$form, FormStateInterface $form_state) {
    $url = \Drupal\Core\Url::fromUri('internal:/');
		$form_state->setRedirectUrl($url);
		
  }

}

<?php

namespace Drupal\custom\Plugin\Field\FieldFormatter;

use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\datetime\Plugin\Field\FieldFormatter\DateTimeFormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\datetime\Plugin\Field\FieldType\DateTimeItemInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Entity\EntityManagerInterface;
use Drupal\Core\Field\FormatterBase;
/**
 * Plugin implementation of the 'Custom' formatter for 'datetime' fields.
 *
 * @FieldFormatter(
 *   id = "custom_namsinh_user",
 *   label = @Translation("Custom Nam Sinh"),
 *   field_types = {
 *     "datetime"
 *   }
 *)
 */
class CustomFieldNamSinhFormat extends DateTimeFormatterBase {

		/**
		 * {@inheritdoc}
		 */
		public static function defaultSettings() {
			return parent::defaultSettings();
		}
	
		/**
		 * {@inheritdoc}
		 */
		public function viewElements(FieldItemListInterface $items, $langcode) {
			$elements = [];
			foreach ($items as $delta => $item) {
				if( $item->getEntity()->field_hien_thi->value == 0 ) {
					return $elements;
				}
				if (!empty($item->date)) {
					/** @var \Drupal\Core\Datetime\DrupalDateTime $date */
					if( $item->getEntity()->field_tuy_ch->value == 2 ) {
						$elements[$delta]['#markup'] = date("m/d/Y", strtotime($item->value));
					} else {
						$elements[$delta]['#markup'] = date("m/d", strtotime($item->value));
					}
				}
			}
			return $elements;
		}

		protected function formatDate($date) {
			$format = $this->getSetting('date_format');
			$timezone = $this->getSetting('timezone_override');
			return $this->dateFormatter->format($date->getTimestamp(), 'custom', $format, $timezone != '' ? $timezone : NULL);
		}
	
		/**
		 * {@inheritdoc}
		 */
		public function settingsForm(array $form, FormStateInterface $form_state) {
			$form = parent::settingsForm($form, $form_state);
	
			return $form;
		}
	
		/**
		 * {@inheritdoc}
		 */
		public function settingsSummary() {
			$summary = parent::settingsSummary();
	
			return $summary;
		}
}
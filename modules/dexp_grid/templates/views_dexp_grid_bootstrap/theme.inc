<?php
/**
 * @file
 * Preprocessors and helper functions to make theming easier.
 */

use Drupal\Component\Utility\Html;
use Drupal\Core\Template\Attribute;
use Drupal\Core\Url;

/**
 * Prepares variables for views grid template.
 *
 * Default template: views-dexp-grid-bootstrap.html.twig.
 *
 * @param array $vars
 *   An associative array containing:
 *   - view: A ViewExecutable object.
 *   - rows: The raw row data.
 */
function template_preprocess_views_dexp_grid_bootstrap(&$vars) {
  $id = Html::getUniqueId($vars['view']->element['#name'] . '-' . $vars['view']->element['#display_id']);
  $vars['id'] = $id;
  $vars['attributes']['id'] = $id;
  $options = $vars['view']->style_plugin->options;
  // Carousel options.
  $vars['lg_cols'] = $options['lg_cols'];
  $vars['md_cols'] = $options['md_cols'];
  $vars['sm_cols'] = $options['sm_cols'];
  $vars['xs_cols'] = $options['xs_cols'];
  $padding = empty($options['col_padding'])?15:$options['col_padding'];
  $items = array();
  // Iterate over each rendered views result row.
  $vars['attributes']['class'][] = 'dexp-grid';
  $vars['attributes']['class'][] = $options['wrapper_class'];
  $vars['attributes']['class'][] = 'dexp-grid-bootstrap';
  $vars['attributes']['class'][] = 'dexp-grid-bootstrap-lg-' . $vars['lg_cols'];
  $vars['attributes']['class'][] = 'dexp-grid-bootstrap-md-' . $vars['md_cols'];
  $vars['attributes']['class'][] = 'dexp-grid-bootstrap-sm-' . $vars['sm_cols'];
  $vars['attributes']['class'][] = 'dexp-grid-bootstrap-xs-' . $vars['xs_cols'];
  
  foreach ($vars['rows'] as $result_index => $item) {
    $row_attributes = array('class' => array('dexp-grid-item'));
    
    $row_attributes['class'][] = 'col-lg-' . 12 / $vars['lg_cols'];
    $row_attributes['class'][] = 'col-md-' . 12 / $vars['md_cols'];
    $row_attributes['class'][] = 'col-sm-' . 12 / $vars['sm_cols'];
    $row_attributes['class'][] = 'col-xs-' . 12 / $vars['xs_cols'];
    if($padding != 15){
      $vars['attributes']['style'][] = 'margin-left: -' . $padding . 'px;';
      $vars['attributes']['style'][] = 'margin-right: -' . $padding . 'px;';
      $row_attributes['style'][] = 'padding-left:' . $padding . 'px;';
      $row_attributes['style'][] = 'padding-right:' . $padding . 'px;';
    }
    $row_attributes['style'][] = 'margin-bottom:' . ($padding * 2) . 'px;';
    $vars['attributes']['style'][] = 'margin-bottom:-' . ($padding * 2) . 'px;';
    $items[] = array(
      'content' => $item,
      'attributes' => new Attribute($row_attributes),
    );
  }
  $vars['items'] = $items;
  $vars['#attached']['library'][] = 'dexp_grid/grid_bootstrap';
}

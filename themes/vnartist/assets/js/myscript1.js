(function ($, Drupal, drupalSettings) {
	"use strict";
	Drupal.behaviors.vnartist = {
		attach: function (context, settings) {
			$('.hamburger', context).click(function(e) {
				e.preventDefault();
                $('.dexp-container').toggleClass('hide-sidebar');
				$('body').toggleClass('hide-sidebar');
			});
		}
	};
})(jQuery, Drupal, drupalSettings);

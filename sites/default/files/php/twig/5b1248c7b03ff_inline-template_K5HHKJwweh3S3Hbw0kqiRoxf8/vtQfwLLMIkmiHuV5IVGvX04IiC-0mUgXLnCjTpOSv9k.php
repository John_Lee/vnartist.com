<?php

/* {# inline_template_start #}{% if field_disable|striptags == '1' %}
<a class="disable" href="">{{ name }}</a>
{% else %}
<a href="{{ view_taxonomy_term }}">{{ name }}</a>
{% endif %} */
class __TwigTemplate_75e59d696bc699cee934cfb5fb1e20d82da8250ddeed7746917e6f04c4249025 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 1);
        $filters = array("striptags" => 1);
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array('striptags'),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        if ((strip_tags((isset($context["field_disable"]) ? $context["field_disable"] : null)) == "1")) {
            // line 2
            echo "<a class=\"disable\" href=\"\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true));
            echo "</a>
";
        } else {
            // line 4
            echo "<a href=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["view_taxonomy_term"]) ? $context["view_taxonomy_term"] : null), "html", null, true));
            echo "\">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true));
            echo "</a>
";
        }
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}{% if field_disable|striptags == '1' %}
<a class=\"disable\" href=\"\">{{ name }}</a>
{% else %}
<a href=\"{{ view_taxonomy_term }}\">{{ name }}</a>
{% endif %}";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 4,  49 => 2,  47 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}{% if field_disable|striptags == '1' %}
<a class=\"disable\" href=\"\">{{ name }}</a>
{% else %}
<a href=\"{{ view_taxonomy_term }}\">{{ name }}</a>
{% endif %}", "{# inline_template_start #}{% if field_disable|striptags == '1' %}
<a class=\"disable\" href=\"\">{{ name }}</a>
{% else %}
<a href=\"{{ view_taxonomy_term }}\">{{ name }}</a>
{% endif %}", "");
    }
}

<?php

/* themes/vnartist/templates/flag.html.twig */
class __TwigTemplate_792a2748df3a730f097dfdbeb7b666865aaa0bb3a5857b5897b4cce830d346d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("spaceless" => 14, "if" => 19, "set" => 20);
        $filters = array("clean_class" => 29, "join" => 45);
        $functions = array("attach_library" => 16, "current_uid" => 43);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('spaceless', 'if', 'set'),
                array('clean_class', 'join'),
                array('attach_library', 'current_uid')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 14
        ob_start();
        // line 16
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->attachLibrary("flag/flag.link"), "html", null, true));
        echo "

";
        // line 19
        if (((isset($context["action"]) ? $context["action"] : null) == "unflag")) {
            // line 20
            echo "    ";
            $context["action_class"] = "action-unflag";
        } else {
            // line 22
            echo "    ";
            $context["action_class"] = "action-flag";
        }
        // line 24
        echo "
";
        // line 27
        $context["classes"] = array(0 => "flag", 1 => ("flag-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 29
(isset($context["flag"]) ? $context["flag"] : null), "id", array(), "method"))), 2 => ((("js-flag-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 30
(isset($context["flag"]) ? $context["flag"] : null), "id", array(), "method"))) . "-") . $this->getAttribute((isset($context["flaggable"]) ? $context["flaggable"] : null), "id", array(), "method")), 3 =>         // line 31
(isset($context["action_class"]) ? $context["action_class"] : null));
        // line 35
        $context["classes_anonymous"] = array(0 => "flag", 1 => ("flag-" . \Drupal\Component\Utility\Html::getClass($this->getAttribute(        // line 37
(isset($context["flag"]) ? $context["flag"] : null), "id", array(), "method"))));
        // line 40
        echo "
";
        // line 42
        $context["attributes"] = $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "setAttribute", array(0 => "rel", 1 => "nofollow"), "method");
        // line 43
        $context["current_uid"] = $this->env->getExtension('Drupal\dexp_vnartist\Twig\DexpVnartistTwigExtension')->current_uid();
        // line 44
        if (((isset($context["current_uid"]) ? $context["current_uid"] : null) == 0)) {
            // line 45
            echo "\t<div class=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_join_filter((isset($context["classes_anonymous"]) ? $context["classes_anonymous"] : null), " "), "html", null, true));
            echo "\">
\t\t<a class=\"use-ajax login-popup-form flag-like-button\" href=\"/user/login\" data-dialog-type='modal' data-dialog-options='{\"width\":350}'>";
            // line 46
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
            echo "</a>
\t\t";
            // line 47
            if (($this->getAttribute((isset($context["flag"]) ? $context["flag"] : null), "id", array(), "method") == "like")) {
                // line 48
                echo "\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["count_l"]) ? $context["count_l"] : null), "html", null, true));
                echo "
\t\t\t";
                // line 49
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["like_list"]) ? $context["like_list"] : null), "html", null, true));
                echo "
\t\t";
            }
            // line 51
            echo "\t</div>
";
        } else {
            // line 53
            echo "\t<div class=\"";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, twig_join_filter((isset($context["classes"]) ? $context["classes"] : null), " "), "html", null, true));
            echo "\">
\t\t<a";
            // line 54
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["attributes"]) ? $context["attributes"] : null), "addClass", array(0 => "flag-like-button"), "method"), "html", null, true));
            echo ">";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true));
            echo "</a>
\t\t";
            // line 55
            if (($this->getAttribute((isset($context["flag"]) ? $context["flag"] : null), "id", array(), "method") == "like")) {
                // line 56
                echo "\t\t\t";
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["count_l"]) ? $context["count_l"] : null), "html", null, true));
                echo "
\t\t\t";
                // line 57
                echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["like_list"]) ? $context["like_list"] : null), "html", null, true));
                echo "
\t\t";
            }
            // line 59
            echo "\t</div>
";
        }
        echo trim(preg_replace('/>\s+</', '><', ob_get_clean()));
    }

    public function getTemplateName()
    {
        return "themes/vnartist/templates/flag.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 59,  123 => 57,  118 => 56,  116 => 55,  110 => 54,  105 => 53,  101 => 51,  96 => 49,  91 => 48,  89 => 47,  85 => 46,  80 => 45,  78 => 44,  76 => 43,  74 => 42,  71 => 40,  69 => 37,  68 => 35,  66 => 31,  65 => 30,  64 => 29,  63 => 27,  60 => 24,  56 => 22,  52 => 20,  50 => 19,  45 => 16,  43 => 14,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 * Default theme implementation for flag links.
 *
 * Available variables:
 * - attributes: HTML attributes for the link element.
 * - title: The flag link title.
 * - action: 'flag' or 'unflag'
 * - flag: The flag object.
 * - flaggable: The flaggable entity.
 */
#}
{% spaceless %}
{# Attach the flag CSS library.#}
{{ attach_library('flag/flag.link') }}

{# Depending on the flag action, set the appropriate action class. #}
{% if action == 'unflag' %}
    {% set action_class = 'action-unflag' %}
{% else %}
    {% set action_class = 'action-flag' %}
{% endif %}

{# Set the remaining Flag CSS classes. #}
{%
  set classes = [
    'flag',
    'flag-' ~ flag.id()|clean_class,
    'js-flag-' ~ flag.id()|clean_class ~ '-' ~ flaggable.id(),
    action_class
  ]
%}
{%
  set classes_anonymous = [
    'flag',
    'flag-' ~ flag.id()|clean_class,
  ]
%}

{# Set nofollow to prevent search bots from crawling anonymous flag links #}
{% set attributes = attributes.setAttribute('rel', 'nofollow') %}
{% set current_uid = current_uid() %}
{% if current_uid == 0 %}
\t<div class=\"{{ classes_anonymous|join(' ') }}\">
\t\t<a class=\"use-ajax login-popup-form flag-like-button\" href=\"/user/login\" data-dialog-type='modal' data-dialog-options='{\"width\":350}'>{{ title }}</a>
\t\t{% if flag.id() == 'like' %}
\t\t\t{{ count_l }}
\t\t\t{{ like_list }}
\t\t{% endif %}
\t</div>
{% else %}
\t<div class=\"{{ classes|join(' ') }}\">
\t\t<a{{ attributes.addClass('flag-like-button') }}>{{ title }}</a>
\t\t{% if flag.id() == 'like' %}
\t\t\t{{ count_l }}
\t\t\t{{ like_list }}
\t\t{% endif %}
\t</div>
{% endif %}
{% endspaceless %}
", "themes/vnartist/templates/flag.html.twig", "/home/admin/domains/vnartist.com/public_html/dev/themes/vnartist/templates/flag.html.twig");
    }
}

<?php

/* {# inline_template_start #}<div class="user-profile">
<span class="user-name">{{ field_ho_va_ten }}</span>
<span class="user-picture">{{ user_picture }}</span>
<div class="list-user">
  <ul class="list-user-inner">
    <li><a href="{{ url('entity.user.canonical', { 'user':uid }) }}"><i class="fa fa-user"></i>
<span>Thông tin người dùng</span></a></li>
    <li><a class="use-ajax" href="/lien-he-admin" data-dialog-type="modal"><i class="fa fa-headphones"></i><span>Liên hệ với admin</span></a></li><a ></a>
    <li><a href="{{ url('user.logout') }}"><i class="fa fa-sign-out"></i><span>Đăng xuất</span></a></li>
  </ul>
</div>
</div> */
class __TwigTemplate_393845661887464c91773ad9962777661ded1d62b14f26b04b2c2c3a5c722339 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array("url" => 6);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array('url')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"user-profile\">
<span class=\"user-name\">";
        // line 2
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["field_ho_va_ten"]) ? $context["field_ho_va_ten"] : null), "html", null, true));
        echo "</span>
<span class=\"user-picture\">";
        // line 3
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["user_picture"]) ? $context["user_picture"] : null), "html", null, true));
        echo "</span>
<div class=\"list-user\">
  <ul class=\"list-user-inner\">
    <li><a href=\"";
        // line 6
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("entity.user.canonical", array("user" => (isset($context["uid"]) ? $context["uid"] : null))), "html", null, true));
        echo "\"><i class=\"fa fa-user\"></i>
<span>Thông tin người dùng</span></a></li>
    <li><a class=\"use-ajax\" href=\"/lien-he-admin\" data-dialog-type=\"modal\"><i class=\"fa fa-headphones\"></i><span>Liên hệ với admin</span></a></li><a ></a>
    <li><a href=\"";
        // line 9
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar($this->env->getExtension('Drupal\Core\Template\TwigExtension')->getUrl("user.logout")));
        echo "\"><i class=\"fa fa-sign-out\"></i><span>Đăng xuất</span></a></li>
  </ul>
</div>
</div>";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"user-profile\">
<span class=\"user-name\">{{ field_ho_va_ten }}</span>
<span class=\"user-picture\">{{ user_picture }}</span>
<div class=\"list-user\">
  <ul class=\"list-user-inner\">
    <li><a href=\"{{ url('entity.user.canonical', { 'user':uid }) }}\"><i class=\"fa fa-user\"></i>
<span>Thông tin người dùng</span></a></li>
    <li><a class=\"use-ajax\" href=\"/lien-he-admin\" data-dialog-type=\"modal\"><i class=\"fa fa-headphones\"></i><span>Liên hệ với admin</span></a></li><a ></a>
    <li><a href=\"{{ url('user.logout') }}\"><i class=\"fa fa-sign-out\"></i><span>Đăng xuất</span></a></li>
  </ul>
</div>
</div>";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 9,  67 => 6,  61 => 3,  57 => 2,  54 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}<div class=\"user-profile\">
<span class=\"user-name\">{{ field_ho_va_ten }}</span>
<span class=\"user-picture\">{{ user_picture }}</span>
<div class=\"list-user\">
  <ul class=\"list-user-inner\">
    <li><a href=\"{{ url('entity.user.canonical', { 'user':uid }) }}\"><i class=\"fa fa-user\"></i>
<span>Thông tin người dùng</span></a></li>
    <li><a class=\"use-ajax\" href=\"/lien-he-admin\" data-dialog-type=\"modal\"><i class=\"fa fa-headphones\"></i><span>Liên hệ với admin</span></a></li><a ></a>
    <li><a href=\"{{ url('user.logout') }}\"><i class=\"fa fa-sign-out\"></i><span>Đăng xuất</span></a></li>
  </ul>
</div>
</div>", "{# inline_template_start #}<div class=\"user-profile\">
<span class=\"user-name\">{{ field_ho_va_ten }}</span>
<span class=\"user-picture\">{{ user_picture }}</span>
<div class=\"list-user\">
  <ul class=\"list-user-inner\">
    <li><a href=\"{{ url('entity.user.canonical', { 'user':uid }) }}\"><i class=\"fa fa-user\"></i>
<span>Thông tin người dùng</span></a></li>
    <li><a class=\"use-ajax\" href=\"/lien-he-admin\" data-dialog-type=\"modal\"><i class=\"fa fa-headphones\"></i><span>Liên hệ với admin</span></a></li><a ></a>
    <li><a href=\"{{ url('user.logout') }}\"><i class=\"fa fa-sign-out\"></i><span>Đăng xuất</span></a></li>
  </ul>
</div>
</div>", "");
    }
}

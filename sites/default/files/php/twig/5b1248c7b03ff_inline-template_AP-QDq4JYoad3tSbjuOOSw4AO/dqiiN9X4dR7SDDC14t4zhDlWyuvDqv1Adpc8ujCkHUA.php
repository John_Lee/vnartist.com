<?php

/* {# inline_template_start #}{% if user_picture is not empty %}
 {{ user_picture }}
{% else %}
 <i class="fa fa-user"></i>
{% endif %} */
class __TwigTemplate_1cc4b0f7b9b696af1fda8c7c7e4e51def64fc47a3915db0f79a19a006093c6c5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 1);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        if ( !twig_test_empty((isset($context["user_picture"]) ? $context["user_picture"] : null))) {
            // line 2
            echo " ";
            echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["user_picture"]) ? $context["user_picture"] : null), "html", null, true));
            echo "
";
        } else {
            // line 4
            echo " <i class=\"fa fa-user\"></i>
";
        }
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}{% if user_picture is not empty %}
 {{ user_picture }}
{% else %}
 <i class=\"fa fa-user\"></i>
{% endif %}";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 4,  49 => 2,  47 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}{% if user_picture is not empty %}
 {{ user_picture }}
{% else %}
 <i class=\"fa fa-user\"></i>
{% endif %}", "{# inline_template_start #}{% if user_picture is not empty %}
 {{ user_picture }}
{% else %}
 <i class=\"fa fa-user\"></i>
{% endif %}", "");
    }
}

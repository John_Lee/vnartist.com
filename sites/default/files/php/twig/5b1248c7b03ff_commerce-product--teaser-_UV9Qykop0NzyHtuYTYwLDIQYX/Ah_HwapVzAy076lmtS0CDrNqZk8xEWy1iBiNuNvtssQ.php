<?php

/* themes/vnartist/templates/commerce/commerce-product--teaser-alt.html.twig */
class __TwigTemplate_7cd9ebcc0a9a20fdb6976cb45a2fec990c7ed85b07b1a26c89ba94363b17e412 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array("getUrlProductStyle" => 22, "product_comment_count" => 27);

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array('getUrlProductStyle', 'product_comment_count')
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 22
        echo "<a class=\"product-img\" href=\"";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, (isset($context["product_url"]) ? $context["product_url"] : null), "html", null, true));
        echo "\"><img class=\"masonry\" alt=\"";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute($this->getAttribute((isset($context["product_entity"]) ? $context["product_entity"] : null), "title", array()), "value", array()), "html", null, true));
        echo "\" src=\"";
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\dexp_vnartist\Twig\DexpVnartistTwigExtension')->getUrlProductStyle("picture_style_masonry", $this->getAttribute($this->getAttribute($this->getAttribute((isset($context["product_entity"]) ? $context["product_entity"] : null), "field_picture_image", array()), "entity", array()), "fileuri", array())), "html", null, true));
        echo "\" width=\"auto\" height=\"320\" /></a>
";
        // line 23
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "title", array()), "html", null, true));
        echo "
";
        // line 24
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "uid", array()), "html", null, true));
        echo "
<div class=\"product-like-comment\">
    <div class=\"product-like\">";
        // line 26
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : null), "flag_like", array()), "html", null, true));
        echo "</div>
    <div class=\"product-comment\"><i class=\"fa fa-comment\"></i>";
        // line 27
        echo $this->env->getExtension('Twig_Extension_Sandbox')->ensureToStringAllowed($this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->env->getExtension('Drupal\dexp_vnartist\Twig\DexpVnartistTwigExtension')->product_comment_count((isset($context["product_entity"]) ? $context["product_entity"] : null)), "html", null, true));
        echo "</div>
</div>";
    }

    public function getTemplateName()
    {
        return "themes/vnartist/templates/commerce/commerce-product--teaser-alt.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 27,  61 => 26,  56 => 24,  52 => 23,  43 => 22,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{#
/**
 * @file
 *
 * Default product template.
 *
 * Available variables:
 * - attributes: HTML attributes for the wrapper.
 * - product: The rendered product fields.
 *   Use 'product' to print them all, or print a subset such as
 *   'product.title'. Use the following code to exclude the
 *   printing of a given field:
 *   @code
 *   {{ product|without('title') }}
 *   @endcode
 * - product_entity: The product entity.
 * - product_url: The product URL.
 *
 * @ingroup themeable
 */
#}
<a class=\"product-img\" href=\"{{ product_url }}\"><img class=\"masonry\" alt=\"{{ product_entity.title.value }}\" src=\"{{ getUrlProductStyle('picture_style_masonry', product_entity.field_picture_image.entity.fileuri) }}\" width=\"auto\" height=\"320\" /></a>
{{ product.title }}
{{ product.uid }}
<div class=\"product-like-comment\">
    <div class=\"product-like\">{{ product.flag_like }}</div>
    <div class=\"product-comment\"><i class=\"fa fa-comment\"></i>{{ product_comment_count(product_entity) }}</div>
</div>", "themes/vnartist/templates/commerce/commerce-product--teaser-alt.html.twig", "/home/admin/domains/vnartist.com/public_html/dev/themes/vnartist/templates/commerce/commerce-product--teaser-alt.html.twig");
    }
}

<?php

/* {# inline_template_start #}<div class="thong-bao-tin-nhan">
<a href="#"><i class="fa fa-envelope"></i> </a> 
<a href="#"><i class="fa fa-bell"></i></a>
</div>
 */
class __TwigTemplate_f27d7cad2f54cf2b2a9c9bbaab115f6b2c780851da3df42bf45ad493bb33e0f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array();
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('Twig_Extension_Sandbox')->checkSecurity(
                array(),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"thong-bao-tin-nhan\">
<a href=\"#\"><i class=\"fa fa-envelope\"></i> </a> 
<a href=\"#\"><i class=\"fa fa-bell\"></i></a>
</div>
";
    }

    public function getTemplateName()
    {
        return "{# inline_template_start #}<div class=\"thong-bao-tin-nhan\">
<a href=\"#\"><i class=\"fa fa-envelope\"></i> </a> 
<a href=\"#\"><i class=\"fa fa-bell\"></i></a>
</div>
";
    }

    public function getDebugInfo()
    {
        return array (  47 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{# inline_template_start #}<div class=\"thong-bao-tin-nhan\">
<a href=\"#\"><i class=\"fa fa-envelope\"></i> </a> 
<a href=\"#\"><i class=\"fa fa-bell\"></i></a>
</div>
", "{# inline_template_start #}<div class=\"thong-bao-tin-nhan\">
<a href=\"#\"><i class=\"fa fa-envelope\"></i> </a> 
<a href=\"#\"><i class=\"fa fa-bell\"></i></a>
</div>
", "");
    }
}

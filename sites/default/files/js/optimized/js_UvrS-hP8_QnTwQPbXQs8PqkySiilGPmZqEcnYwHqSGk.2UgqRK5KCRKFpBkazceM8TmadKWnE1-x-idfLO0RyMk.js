/* Source and licensing information for the line(s) below can be found at https://vnartist.com/modules/dexp_builder/assets/js/dexp-colorpicker.js. */
(function($, Drupal){
  Drupal.behaviors.color_picker = {
    attach: function(){
      $('input.color').once('colorpicker').each(function(){
        var $this = $(this);
        //var picker = $('<span class="color-picker"></span>');
        $(this).colorPicker({
          renderCallback: function($elm, toggled) {
            //console.log('ok');
            //console.log($elm);
            if($elm.val() != ''){
              $this.val(this.color.toString('HEX'));
            }
          }
        });
      });
    }
  };
})(jQuery, Drupal);
/* Source and licensing information for the above line(s) can be found at https://vnartist.com/modules/dexp_builder/assets/js/dexp-colorpicker.js. */
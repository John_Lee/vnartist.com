/* Source and licensing information for the line(s) below can be found at https://vnartist.com/modules/colorbox_inline/js/colorbox_inline.js. */
(function ($) {
  "use strict";
  /**
   * Enable the colorbox inline functionality.
   */
  Drupal.behaviors.colorboxInline = {
    attach: function (context, drupalSettings) {
      $('[data-colorbox-inline]', context).once().click(function () {
        var $link = $(this);
        var settings = $.extend({}, drupalSettings.colorbox, {
          href: false,
          inline: true
        }, {
          className: $link.data('class'),
          href: $link.data('colorbox-inline'),
          width: $link.data('width'),
          height: $link.data('height')
        });
        $link.colorbox(settings);
      });
    }
  };
})(jQuery);

/* Source and licensing information for the above line(s) can be found at https://vnartist.com/modules/colorbox_inline/js/colorbox_inline.js. */
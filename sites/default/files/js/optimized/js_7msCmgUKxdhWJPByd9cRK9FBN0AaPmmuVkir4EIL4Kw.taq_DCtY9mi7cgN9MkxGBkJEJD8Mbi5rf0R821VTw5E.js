/* Source and licensing information for the line(s) below can be found at https://vnartist.com/modules/colorbox/styles/default/colorbox_style.js. */
(function ($) {

Drupal.behaviors.initColorboxDefaultStyle = {
  attach: function (context, settings) {
    $(context).bind('cbox_complete', function () {
      // Only run if there is a title.
      if ($('#cboxTitle:empty', context).length == false) {
        $('#cboxLoadedContent img', context).bind('mouseover', function () {
          $('#cboxTitle', context).slideDown();
        });
        $('#cboxOverlay', context).bind('mouseover', function () {
          $('#cboxTitle', context).slideUp();
        });
      }
      else {
        $('#cboxTitle', context).hide();
      }
    });
  }
};

})(jQuery);

/* Source and licensing information for the above line(s) can be found at https://vnartist.com/modules/colorbox/styles/default/colorbox_style.js. */
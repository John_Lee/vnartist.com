/* Source and licensing information for the line(s) below can be found at http://vnartist.com/modules/dexp_block_settings/assets/js/dexp.animate.js. */
(function ($, Drupal, settings) {
  "use strict";

  Drupal.behaviors.dexp_block_settings_animate = {
    attach: function () {
      if (settings.dexp_block_settings.disbale_animation) {
        $(".dexp-animate").once('animate').each(function () {
          $(this).removeClass('dexp-animate').removeClass('animated');
        });
      } else if (settings.dexp_block_settings.disbale_animation_mobile && $(window).width() < 768) {
        $(".dexp-animate").once('animate').each(function () {
          $(this).removeClass('dexp-animate').removeClass('animated');
        });
      } else {
        $(".dexp-animate").once('animate').each(function () {
          var $this = $(this);
          var animate = $(this).data('animate');
          var delay = $(this).data('animate-delay') || 0;
          $this.appear(function () {
            setTimeout(function () {
              $this.addClass(animate).addClass('dexp-animated');
            }, delay);
          }, {
            accX: 0,
            accY: 0,
            one: true
          });
        });
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
/* Source and licensing information for the above line(s) can be found at http://vnartist.com/modules/dexp_block_settings/assets/js/dexp.animate.js. */
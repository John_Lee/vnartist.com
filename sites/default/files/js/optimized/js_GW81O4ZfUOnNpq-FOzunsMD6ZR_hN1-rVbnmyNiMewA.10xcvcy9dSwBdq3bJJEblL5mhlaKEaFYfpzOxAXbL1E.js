/* Source and licensing information for the line(s) below can be found at http://vnartist.com/themes/vnartist/assets/js/grid-masonry.js. */
(function ($, Drupal, drupalSettings) {
	"use strict";
	Drupal.behaviors.vnartist_grid_masonry = {
		attach: function (context, settings) {
            $(window).on('load', function(){
                var ww = $(window).width();
                if(ww > 480){
                    setTimeout(function(){
                        $(".view-grid-masonry .view-content").rowGrid({
                            itemSelector: ".views-row",
                            minMargin: 20,
                            firstItemClass: "first-item",
                            lastItemClass: "last-item",
                            resize: true,
                        });
                    }, 50);
                }
                setTimeout(function(){
                    $(".view-grid-masonry .views-row").once('check-width').each(function(){
                        var piw = $(this).find('.product-img img').width(),
                            ppw = $(this).find('.profile').width(),
                            plw = $(this).find('.product-like-comment').width();
                        if(piw < (ppw + plw + 45)){
                            $(this).closest('.views-row').addClass('product-small-width');
                        }
                    });
                }, 100);
            });
		}
	};
})(jQuery, Drupal, drupalSettings);

/* Source and licensing information for the above line(s) can be found at http://vnartist.com/themes/vnartist/assets/js/grid-masonry.js. */
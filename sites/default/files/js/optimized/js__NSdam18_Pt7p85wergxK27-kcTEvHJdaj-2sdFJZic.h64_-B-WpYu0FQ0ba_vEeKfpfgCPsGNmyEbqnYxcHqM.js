/* Source and licensing information for the line(s) below can be found at https://vnartist.com/modules/dexp_image_browser/assets/js/image_browser.js. */
(function ($, Drupal, settings) {
  "use strict";
  
  Drupal.behaviors.dexp_image_browser = {
    attach: function (context, settings) {
      $('.image-browser', context).find('input.image-selector').once('click').each(function(){
        $(this).click(function(e){
          e.preventDefault();
          $('.image-browser').removeClass('active');
          $(this).closest('.image-browser').addClass('active');
          Drupal.ajax({
            url: Drupal.url('dexp_image_browser/image_browser'),
            dialog: {
              width: '80%',
              height: 'auto',
              title: 'Choose Image'
            },
            dialogType: 'modal'
          }).execute();
        });
      });
      $('.image-browser', context).find('input.image-remove').once('click').each(function(){
        $(this).click(function(e){
          e.preventDefault();
          $(this).closest('.image-browser').find('input.form-image-browser').val('file:0').trigger('update');
          $(this).closest('.image-browser').removeClass('has-image');
        });
      });
      $('ul.dexp-image-browser-tabs').find('a:first').once('click').trigger('click');
    }
  };
})(jQuery, Drupal, drupalSettings);
/* Source and licensing information for the above line(s) can be found at https://vnartist.com/modules/dexp_image_browser/assets/js/image_browser.js. */
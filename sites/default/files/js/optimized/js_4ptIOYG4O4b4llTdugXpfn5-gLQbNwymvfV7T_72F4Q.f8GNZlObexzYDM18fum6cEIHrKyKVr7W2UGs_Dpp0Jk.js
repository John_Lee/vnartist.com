/* Source and licensing information for the line(s) below can be found at https://vnartist.com/themes/vnartist/assets/js/myscript1.js. */
(function ($, Drupal, drupalSettings) {
	"use strict";
	Drupal.behaviors.vnartist = {
		attach: function (context, settings) {
			$('.hamburger', context).click(function(e) {
				e.preventDefault();
                $('.dexp-container').toggleClass('hide-sidebar');
				$('body').toggleClass('hide-sidebar');
			});
		}
	};
})(jQuery, Drupal, drupalSettings);

/* Source and licensing information for the above line(s) can be found at https://vnartist.com/themes/vnartist/assets/js/myscript1.js. */
/* Source and licensing information for the line(s) below can be found at https://www.vnartist.com/modules/dexp_builder/assets/js/custom_css.js. */
(function($, Drupal){
  Drupal.behaviors.custom_css = {
    attach: function(){
      $('.custom-css-button').once('click').each(function(){
        $(this).click(function(e){
          e.preventDefault();
          Drupal.dexp_builder.ajax({
            url: Drupal.url('dexp_builder/custom_css'),
            dialogType: 'modal',
            dialog: {
              width: '80%',
              selector: '#custom_css'
            }
          }).execute();
        });
      });
    }
  };
})(jQuery, Drupal);
/* Source and licensing information for the above line(s) can be found at https://www.vnartist.com/modules/dexp_builder/assets/js/custom_css.js. */
/* Source and licensing information for the line(s) below can be found at https://www.vnartist.com/modules/custom/assets/js/file_upload.js. */
(function ($, Drupal) {
	
  Drupal.behaviors.custom = {
		
    attach: function (context, settings) {
			
    }
  };
})(jQuery, Drupal);

jQuery(document).ready(function($) {
  $('.wrapper-image-picture').hide();

  console.log($('.js-select2-categories').data('title-placeholder'))
  $('.js-select2-categories').select2({
    placeholder: $(this).data('title-placeholder'),
  });

  $('.js-select2-categories').each(function() {
    var title = $(this).data('title-placeholder');
    $(this).select2({
      placeholder: title,
    });
  })

  $('.wrapper-form-add-picture.form-step-1 .button').attr("disabled", "disabled");
	$(".custom-input-upload-file").dropzone({
		url: "/ajax_upload_file",
		maxFiles: 1,
    addRemoveLinks: true,
    acceptedFiles: "image/*",
    init: function() {
        this.on("addedfile", function(file) {
          $('.custom-input-upload-file .dz-message').hide();
        }),
        this.on("success", function(file, response) {
            $('.wrapper-form-add-picture.form-step-1 .button').removeAttr("disabled");
            $('input[name="picture_file"]').val( response[0]['data'].fid );
            var image = '<img src="'+ response[0]['data'].file_url +'" />';//
            $('.wrapper-image-picture .content-image').html(image);
            $('.wrapper-image-picture').show();
            
        }),
        this.on("removedfile", function(file) {
          $('.custom-input-upload-file .dz-message').show();
          $('input[name="picture_file"]').val('');
          $('.wrapper-form-add-picture.form-step-1 .button').attr("disabled", "disabled");
          $('.wrapper-image-picture .content-image').html('');
          $('.wrapper-image-picture').hide();
        }),
        this.on("uploadprogress", function(file) {
          
        })
    }
	});
});
/* Source and licensing information for the above line(s) can be found at https://www.vnartist.com/modules/custom/assets/js/file_upload.js. */
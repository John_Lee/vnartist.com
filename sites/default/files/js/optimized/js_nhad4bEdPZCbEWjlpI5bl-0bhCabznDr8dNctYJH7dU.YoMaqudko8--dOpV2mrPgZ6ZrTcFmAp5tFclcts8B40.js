/* Source and licensing information for the line(s) below can be found at http://vnartist.com/core/modules/statistics/statistics.js. */
/**
* DO NOT EDIT THIS FILE.
* See the following change record for more information,
* https://www.drupal.org/node/2815083
* @preserve
**/

(function ($, Drupal, drupalSettings) {
  $(document).ready(function () {
    $.ajax({
      type: 'POST',
      cache: false,
      url: drupalSettings.statistics.url,
      data: drupalSettings.statistics.data
    });
  });
})(jQuery, Drupal, drupalSettings);
/* Source and licensing information for the above line(s) can be found at http://vnartist.com/core/modules/statistics/statistics.js. */
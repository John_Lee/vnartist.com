/* Source and licensing information for the line(s) below can be found at https://vnartist.com/themes/vnartist/assets/js/myscript.js. */
(function ($, Drupal, drupalSettings) {
	"use strict";
	Drupal.behaviors.vnartist_theme_js = {
		attach: function (context, settings) {
            var ww = $(window).width();
			// set active for categories
			var url = (window.location.href).split('/');
			$('.view-categories').once('link-categories').find('.item-list li a').each(function(){
				var href = ($(this).attr('href')).split('/');
				if(url[url.length - 2] === href[href.length - 2]){
					if(url[url.length - 1] === href[href.length - 1]){
						$(this).addClass('is-active');
					}
				}
			});
            $('.tranh-hot-theo-doi').once('link-tab').find('a').each(function(){
                $(this).removeClass('active');
                var href = ($(this).attr('href')).split('/');
                if(url[url.length - 1] === href[href.length - 1]){
                    $(this).addClass('active');
                }else if(url[url.length - 1] == ''){
                    $('.tranh-hot-theo-doi').find('.tranh-hot').addClass('active');
                }
            });
            $('a.disable').once('link-disable').click(function(e){
                e.preventDefault();
            });
			// set value for unit price
			$("input[name*='unit_price[0][amount][number]']").val('1');
			//set follow for anonymous user
			if(!$('body').hasClass('user-logged-in')){
				$('.btn-submit-comment').once('btn-comment').each(function(){
					$(this).parent().css('position', 'relative');
					$(this).parent().append('<a class="btn-submit-comment-anonymous use-ajax login-popup-form" href="/user/login" data-dialog-type=\'modal\' data-dialog-options=\'{"width":350}\'></a>');
				});
			} 
            $('.single-product .product-description').once('product-des').each(function(){
                var limitW = 320;
                if(ww < 768){
                    limitW = 100;
                }
                var char = 4;
                var txt = $(this).find('.field--name-field-description').html();
                if(txt){
                    var txtStart = txt.slice(0,limitW).replace(/\w+$/,'');
                    var txtEnd = txt.slice(txtStart.length);
                    if ( txtEnd.replace(/\s+$/,'').split(' ').length > char ) {
                        $(this).find('.field--name-field-description').html([
                            txtStart,
                            '<a href="#" class="more">...xem thêm</a>',
                            '<span class="detail">',
                            txtEnd,
                            '</span>'
                        ].join('')
                      );
                    }
                }
                $('span.detail').hide();
                $('a.more').click(function() {
                    $(this).hide().next('span.detail').fadeIn();
                    if(ww < 768){
                        var ch = $('.single-product .product-content-mobile').outerHeight();
                        $('.single-product .user-proccess').css('top', ch - 70 );
                    }
                    return false;
                });
            });
            if(ww < 768){
                /* hide sidebar */
                $('.dexp-container').addClass('hide-sidebar');
				$('body').addClass('hide-sidebar');
                
                $('.region-logo').find('.user-picture a').removeAttr('href');
                $('.region-logo .user-picture').once('profile-click').click(function(){
                    $('.region-logo .list-user').toggleClass('open');
                });
                $(document).once('doc-click').click(function(e){
                    var user_profile = $('.region-logo .list-user');
                    var user_link = $('.region-logo .user-picture');
                    if ( user_profile.is(':visible') && !user_profile.is(e.target) && user_profile.has(e.target).length === 0 && !user_link.is(e.target) && user_link.has(e.target).length === 0){
                        user_profile.removeClass('open');
                    }
                });
                $('.single-product .product-content-mobile').once('product-content').each(function(){
                    var ch = $(this).outerHeight();
                    $('.single-product .user-proccess').css('top', ch - 65 );
                });
            } else {
                $('.user-profile').hover(function(){
                    $(this).find('.list-user').addClass('open');
                }, function() {
                    $(this).find('.list-user').removeClass('open');
                });
            }
            $('.profile-detail .action-flag a').once('follow').click(function(){
                var number = parseInt($(this).closest('.profile-detail').find('.count-user-follow .number').html()) + 1;
                $(this).closest('.profile-detail').find('.count-user-follow .number').html(number);
            });
            $('.profile-detail .action-unflag a').once('unfollow').click(function(){
                var number = parseInt($(this).closest('.profile-detail').find('.count-user-follow .number').html()) - 1;
                $(this).closest('.profile-detail').find('.count-user-follow .number').html(number);
            });
            $(window).once('dialog-aftercreate').on('dialog:aftercreate', function () {
                $('.ui-dialog').once('link-register').each(function(){
                    var $this = $(this);
                    var link_register = $this.find('.use-login-link-register').clone();
                    $('.use-login-link-register').remove();
                    $this.find('.ui-dialog-buttonset').append(link_register);
                });
            });
            $('.region-sidebar-first').once('check-display').each(function(){
                var display = false;
                $('.region-sidebar-first > div').each(function(){
                    if($(this).is(':visible')){
                        display = true;
                    }
                });
                if(display == false){
                    $(this).hide();
                }
            });
            $('#section-content').once('img-fullsize').each(function(){
                var html = $('.product-image-fullsize').clone();
                $('.product-image-fullsize').remove();
                $(this).prepend(html);
            });
            $('.product-image-single .img-responsive').once('img-responsive').click(function(){
                $(this).hide();
				$('.product-image-single').addClass('zoom-image');
                $('.product-image-fullsize .img-fullsize').show();
            });
            $('.product-image-fullsize .img-fullsize').once('img-fullsize').click(function(){
                $(this).hide();
				$('.product-image-single').removeClass('zoom-image');
                $('.product-image-single .img-responsive').show();
            });
            $('.profile-detail .album-nav a').once('album-nav').click(function(e){
                e.preventDefault();
                var $this = $(this);
                var data = $this.data('product');
                $this.closest('.album-nav').find('a').removeClass('active');
                $this.addClass('active');
                $this.closest('.album-picture').removeClass('profile-picture product-wishlist').addClass(data); 
            });
			$('.count-user-follow').once('user-follow').click(function(){
				$(this).parent().toggleClass('list-open');
			});
			$('.count-follow-user').once('follow-user').click(function(){
				$(this).parent().toggleClass('list-open');
			});
			$('.flag-like .count').once('like-product').click(function(){
				$(this).parent().toggleClass('list-open');
			});
			$(document).once('doc-user-click').click(function(e){
				var user_follow_list = $('.user-follow-list'),
					user_follow = $('.count-user-follow'),
					follow_user_list = $('.follow-user-list'),
					follow_user = $('.count-follow-user'),
					like_list = $('.flag-like .like-list'),
					like_product = $('.flag-like .count');
				if ( user_follow_list.is(':visible') && !user_follow_list.is(e.target) && user_follow_list.has(e.target).length === 0 && !user_follow.is(e.target) && user_follow.has(e.target).length === 0){
					user_follow_list.parent().removeClass('list-open');
				}
				if ( follow_user_list.is(':visible') && !follow_user_list.is(e.target) && follow_user_list.has(e.target).length === 0 && !follow_user.is(e.target) && follow_user.has(e.target).length === 0){
					follow_user_list.parent().removeClass('list-open');
				}
				if ( like_list.is(':visible') && !like_list.is(e.target) && like_list.has(e.target).length === 0 && !like_product.is(e.target) && like_product.has(e.target).length === 0){
					like_list.parent().removeClass('list-open');
				}
			});
			$(document).once('check-ajax').ajaxComplete(function(event, xhr, settings) {
				if($(event.target.activeElement).hasClass('btn-submit-comment') == true || $(event.target.activeElement).hasClass('btn-submit-search') == true){
					location.reload();
				}
			});
		}
	};
})(jQuery, Drupal, drupalSettings);

/* Source and licensing information for the above line(s) can be found at https://vnartist.com/themes/vnartist/assets/js/myscript.js. */
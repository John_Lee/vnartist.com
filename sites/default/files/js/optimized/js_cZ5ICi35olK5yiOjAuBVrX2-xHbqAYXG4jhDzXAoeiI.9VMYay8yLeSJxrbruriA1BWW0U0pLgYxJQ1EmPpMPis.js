/* Source and licensing information for the line(s) below can be found at http://vnartist.com/modules/dexp_vnartist/assets/js/dexp_vnartist.js. */
(function ($, Drupal) {
	"use strict";
	Drupal.behaviors.dexp_vnartist = {
		attach: function (context, settings) {
			$(document).once('check-ajax-follow').ajaxComplete(function(event, xhr, settings) {
				if(settings.url.indexOf('flag/flag/following/') != -1 || 
				   settings.url.indexOf('flag/flag/following/') != -1
				){
					$.ajax({
						url: Drupal.url('ajax_follow'),
						type: 'POST',
						dataType: 'json',
						success: function (results) {
							console.log(results);
							console.log('1');
						}
					});
				}
			});
		}
	};
})(jQuery, Drupal);

/* Source and licensing information for the above line(s) can be found at http://vnartist.com/modules/dexp_vnartist/assets/js/dexp_vnartist.js. */
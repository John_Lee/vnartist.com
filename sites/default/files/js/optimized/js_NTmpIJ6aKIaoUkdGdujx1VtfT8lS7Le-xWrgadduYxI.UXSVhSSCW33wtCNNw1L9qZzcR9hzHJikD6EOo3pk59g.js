/* Source and licensing information for the line(s) below can be found at https://vnartist.com/modules/dropdown_language/js/dropdown_language_selector.js. */
/**
 * @file
 * Dropdown language selector js.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.dropdownLanguagesSelector = {
    attach: function(context, settings) {
      $('.dropdown-language-item .active-language', context).once('dropdown-language-selector')
        .click(function(e) {
          var $wrapper = $(this).closest('.dropbutton-wrapper');
          if ($wrapper.length > 0) {
            e.preventDefault();
            e.stopPropagation();
            $wrapper.toggleClass('open');
          }
        });
    }
  };

})(jQuery, Drupal);

/* Source and licensing information for the above line(s) can be found at https://vnartist.com/modules/dropdown_language/js/dropdown_language_selector.js. */
# Vietnamese translation of AJAX Comments (8.x-1.0-beta1)
# Copyright (c) 2016 by the Vietnamese translation team
#
msgid ""
msgstr ""
"Project-Id-Version: AJAX Comments (8.x-1.0-beta1)\n"
"POT-Creation-Date: 2016-10-19 01:45+0000\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Language-Team: Vietnamese\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

msgid "Cancel"
msgstr "Hủy bỏ"
msgid "Add new comment"
msgstr "Viết bình luận"
msgid "Delete comment"
msgstr "Xóa bài bình luận"
msgid "Your comment has been posted."
msgstr "Bình luận của bạn đã được gửi."
